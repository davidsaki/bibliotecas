import { Table, Button  } from "reactstrap";

const TablaLibro = ({ data, eliminarLibro }) => {
    return (
        <Table striped responsive>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Titulo</th>
                    <th>Autor</th>
                </tr>
            </thead>
            <tbody>
                {
                    (data.length < 1) ? (
                        <tr>
                            <td colSpan="4">Sin registros</td>
                        </tr>
                    ) : (
                            data.map((item) => (
                                <tr key={item.id}>
                                    <td> {item.id} </td>
                                    <td> {item.titulo} </td>
                                    <td> {item.autor} </td>
                                    <td>
                                        <Button color="danger" size="sm" onClick={() => eliminarLibro(item.id)}>Eliminar</Button>
                                    </td>
                                </tr>
                            ))
                    )
                } 
            </tbody>
        </Table>
    )
}

export default TablaLibro;