import { useState } from "react";
import { Modal, Form, FormGroup, Input, Label, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";

const modeloLibro = {
    id: 0,
    titulo: "",
    autor: ""
}

const ModalLibro = ({ mostrarModal, setMostrarModal, guardarLibro }) => {

    const [libro, setLibro] = useState(modeloLibro);

    const actualizarDato = (e) => {
        console.log(e.target.name + " : " + e.target.value);
        setLibro(
            {
                ...libro,
                [e.target.name]: e.target.value
            }
        )
    }

    const enviarDatos = () => {
        if (libro.id == 0) {
            guardarLibro(libro)
        }
    }

    return (
        <Modal isOpen={mostrarModal}>
            <ModalHeader>Nuevo Libro</ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label>Titulo</Label>
                        <Input name="titulo" onChange={(e) => actualizarDato(e)} value={ libro.titulo }/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Autor</Label>
                        <Input name="autor" onChange={(e) => actualizarDato(e)} value={libro.autor} />
                    </FormGroup>
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" size="sm" onClick={ enviarDatos }>Guardar</Button>
                <Button color="danger" size="sm" onClick={() => setMostrarModal(!mostrarModal) }>Cancelar</Button>
            </ModalFooter>
        </Modal>
    )
}

export default ModalLibro;