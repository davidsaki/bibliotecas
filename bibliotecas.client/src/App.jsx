import { Container, Card, CardBody, CardHeader, Col, Row, Button } from "reactstrap";
import TablaLibro from "./Componentes/TablaLibro";
//import './App.css';
//import './index.css';
import { useEffect, useState } from "react";
import ModalLibro from "./Componentes/ModalLibro";
//import { Container, Card, CardBody, CardHeader, Col, Row, Button } from '../node_modules/reactstrap';

const App = () => {

    const [libros, setLibros] = useState([])
    const [mostrarModal, setMostrarModal] = useState(false);

    const mostrarLibros = async () => {
        const response = await fetch("api/libro/listar");
        console.log("response");
        console.log(response);
        if (response.ok) {
            const data = await response.json();
            setLibros(data);
        }
        else {
            console.log("error en la lista");
        }
    }

    useEffect(() => {
        mostrarLibros()
    }, [])

    const guardarLibro = async (libro) => {
        const response = await fetch("api/libro/guardar", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(libro)
        })

        if (response.ok) {
            setMostrarModal(!mostrarModal)
            mostrarLibros();
        }
    }

    const eliminarLibro = async (id) => {
        console.log(id);
        var respuesta = window.confirm("Desea en realidad eliminar este libro?")
        if (!respuesta) {
            return;
        }

        const response = await fetch("api/libro/eliminar/" + id, {
            method: 'DELETE'
        })

        if (response.ok) {
            mostrarLibros();
        }
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col sm="12">
                    <Card>
                        <CardHeader>
                            <h5>Lista de libros</h5>
                        </CardHeader>
                        <CardBody>
                            <Button size="sm" color="success" onClick={() => setMostrarModal(!mostrarModal)}>
                                Nuevo libro
                            </Button>
                            <hr></hr>
                            <TablaLibro data={libros} eliminarLibro={eliminarLibro}></TablaLibro>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            <ModalLibro mostrarModal={mostrarModal} setMostrarModal={setMostrarModal} guardarLibro={guardarLibro} />
        </Container>
    )
}

export default App;
