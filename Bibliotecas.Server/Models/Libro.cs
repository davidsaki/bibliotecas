﻿using System;
using System.Collections.Generic;

namespace Bibliotecas.Server.Models;

public partial class Libro
{
    public int Id { get; set; }

    public string Titulo { get; set; } = null!;

    public string Autor { get; set; } = null!;

    public virtual ICollection<Ejemplar> Ejemplars { get; set; } = new List<Ejemplar>();
}
