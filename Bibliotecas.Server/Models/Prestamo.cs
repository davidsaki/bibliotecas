﻿using System;
using System.Collections.Generic;

namespace Bibliotecas.Server.Models;

public partial class Prestamo
{
    public int Id { get; set; }

    public int IdEjemplar { get; set; }

    public int IdUsuario { get; set; }

    public DateTime? FechaPrestamo { get; set; }

    public DateTime? FechaDevolucion { get; set; }

    public virtual Ejemplar IdEjemplarNavigation { get; set; } = null!;

    public virtual Usuario IdUsuarioNavigation { get; set; } = null!;
}
