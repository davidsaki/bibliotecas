﻿using System;
using System.Collections.Generic;

namespace Bibliotecas.Server.Models;

public partial class Ejemplar
{
    public int Id { get; set; }

    public int IdLibro { get; set; }

    public int Estado { get; set; }

    public virtual Libro IdLibroNavigation { get; set; } = null!;

    public virtual ICollection<Prestamo> Prestamos { get; set; } = new List<Prestamo>();
}
