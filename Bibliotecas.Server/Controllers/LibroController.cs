﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Bibliotecas.Server.Models;
using Microsoft.EntityFrameworkCore;

namespace Bibliotecas.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibroController : ControllerBase
    {
        private readonly BibliotecasContext _dbcontext;

        public LibroController(BibliotecasContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        /// <summary>
        /// Metodo para listar los libros existentes
        /// </summary>
        /// <returns>Lista con los libros existentes</returns>
        [HttpGet]
        [Route("Listar")]
        public async Task<IActionResult> Listar()
        {
            List<Libro> lista = await _dbcontext.Libros.ToListAsync();
            return StatusCode(StatusCodes.Status200OK, lista);
        }

        /// <summary>
        /// Guardar un nuevo libro
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Codigo 200 cuando es guardado correctamente</returns>
        [HttpPost]
        [Route("Guardar")]
        public async Task<IActionResult> Guardar([FromBody] Libro request)
        {
            await _dbcontext.Libros.AddAsync(request);
            await _dbcontext.SaveChangesAsync();
            return StatusCode(StatusCodes.Status200OK, "Ok");
        }

        /// <summary>
        /// Metodo para eliminar un libro
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Codigo 200 cuando es eliminado correctamente</returns>
        [HttpDelete]
        [Route("Eliminar/{id:int}")]
        public async Task<IActionResult> Eliminar(int id)
        {
            Libro libro = _dbcontext.Libros.Find(id);
            if (libro == null)
                return StatusCode(StatusCodes.Status204NoContent, "Id Libro no existe");

            _dbcontext.Libros.Remove(libro);
            await _dbcontext.SaveChangesAsync();

            return StatusCode(StatusCodes.Status200OK, "Ok");
        }

    }
}
