﻿using Bibliotecas.Server.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Bibliotecas.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrestamoController : ControllerBase
    {
        private readonly BibliotecasContext _dbcontext;

        public PrestamoController(BibliotecasContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        /// <summary>
        /// Listar informacion de prestamos
        /// </summary>
        /// <returns>Lista de prestamos</returns>
        [HttpGet]
        [Route("Listar")]
        public async Task<IActionResult> Listar()
        {
            List<Prestamo> lista = await _dbcontext.Prestamos.ToListAsync();
            return StatusCode(StatusCodes.Status200OK, lista);
        }

        /// <summary>
        /// Prestar un libro
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Ok en caso exitoso</returns>
        [HttpPost]
        [Route("Prestar")]
        public async Task<IActionResult> Prestar([FromBody] Prestamo request)
        {
            await _dbcontext.Prestamos.AddAsync(request);
            await _dbcontext.SaveChangesAsync();
            return StatusCode(StatusCodes.Status200OK, "Ok");
        }

        /// <summary>
        /// Devolver un libro
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Ok en caso exitoso</returns>
        [HttpPut]
        [Route("Devolver")]
        public async Task<IActionResult> Devolver([FromBody] Prestamo request)
        {
            request.FechaDevolucion = DateTime.Now;
            _dbcontext.Prestamos.Update(request);
            Ejemplar ejemplar = _dbcontext.Ejemplars.Find(request.IdEjemplar);
            ejemplar.Estado = 0;
            _dbcontext.Ejemplars.Update(ejemplar);
            await _dbcontext.SaveChangesAsync();
            return StatusCode(StatusCodes.Status200OK, "Ok");
        }

    }
}
